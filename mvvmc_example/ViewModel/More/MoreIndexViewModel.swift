//
//  MoreIndexViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class MoreIndexViewModel: BaseViewModel {

  weak var coordinator: MoreCoordinator?
}

extension MoreIndexViewModel: Coordinated { }
