//
//  ViewModelable.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/6.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

protocol ViewModelable: class {
  associatedtype ViewModel
  var viewModel: ViewModel? { get set }
}

extension ViewModelable where Self: UIViewController {
  
  func bindViewModel<ViewModel>(_ viewModel: ViewModel) -> Self {
    self.viewModel = viewModel as? Self.ViewModel
    return self
  }
}

