//
//  TransferMoneyPushViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class TransferMoneyPushViewModel: BaseViewModel {
  
  weak var coordinator: TransferMoneyCoordinator?
  
}

extension TransferMoneyPushViewModel: Coordinated { }
