//
//  LoginViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  
  @IBOutlet weak var coordinatorLabel: UILabel!
  var viewModel: LoginViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  @IBAction func pressedLoginButton(_ sender: UIButton) {
    viewModel?.coordinator?.toNext()
  }
  
  @IBAction func pressedEmailVerifyButton(_ sender: UIButton) {
    viewModel?.coordinator?.startEmailVerifyCoordinator()
  }
}

extension LoginViewController: Storyboarded { }
extension LoginViewController: ViewModelable { }
