//
//  TransferMoneyIndexViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class TransferMoneyIndexViewModel: BaseViewModel {

  weak var coordinator: TransferMoneyCoordinator?
}

extension TransferMoneyIndexViewModel: Coordinated { }
