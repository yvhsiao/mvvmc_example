//
//  TransferMoneyPushViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class TransferMoneyPushViewController: UIViewController {
  
  var viewModel: TransferMoneyPushViewModel?
  var leftBarButtonItems: [UIBarButtonItem] = []
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
}

extension TransferMoneyPushViewController: Storyboarded { }
extension TransferMoneyPushViewController: ViewModelable { }

extension TransferMoneyPushViewController: LeftBarButtonItemable {

  func pressedLeftBarButtonItems(_ sender: UIBarButtonItem) {
    viewModel?.coordinator?.toPrevious()
  }
}
