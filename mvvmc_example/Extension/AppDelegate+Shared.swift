//
//  AppDelegate+Shared.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

extension AppDelegate {
  
  static var shared: AppDelegate? {
    UIApplication.shared.delegate as? AppDelegate
  }
}
