//
//  AccountCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class AccountCoordinator {

  weak var parentCoordinator: Coordinator?
  
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  
  private let barItemType: HomeViewModel.BarItemType
  
  init(parentCoordinator: Coordinator, barItemType: HomeViewModel.BarItemType) {

    self.barItemType = barItemType
    self.parentCoordinator = parentCoordinator

    setupNavigationBar()
  }
  
  func makeAccountPresentVC() -> UIViewController {
    return AccountPresentViewController
      .instantiate("Account")
      .bindViewModel(AccountPresentViewModel().bindCoordinator(self))
  }
  
  func setupNavigationBar() {
    navigationController?.navigationBar.isTranslucent = false
    navigationController?.tabBarItem.title = barItemType.title
    navigationController?.tabBarItem.image = UIImage(named: barItemType.title)
  }
}

extension AccountCoordinator: Coordinator {

  func start() {

    let vc = AccountIndexViewController
      .instantiate("Account")
      .bindViewModel(AccountIndexViewModel().bindCoordinator(self))

    navigationController?.setViewControllers([vc], animated: false)
  }
  
  func toNext() {
  }
  
  func toPrevious() {
  }
}
