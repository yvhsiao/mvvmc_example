//
//  Coordinated.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/5.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

protocol Coordinated: class {
  associatedtype Coordinator
  var coordinator: Coordinator? { get set }
}

extension Coordinated where Self: BaseViewModel {
  
  func bindCoordinator(_ coordinator: Coordinator) -> Self {
    self.coordinator = coordinator
    return self
  }
}
