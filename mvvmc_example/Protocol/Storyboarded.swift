//
//  Storyboarded.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/6.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

protocol Storyboarded {
  
  static func instantiate(_ storyboardName: String) -> Self
}

extension Storyboarded where Self: UIViewController {
  
  static func instantiate(_ storyboardName: String) -> Self {
    let id = String(describing: self)
    let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
    return storyboard.instantiateViewController(withIdentifier: id) as! Self
  }
}
