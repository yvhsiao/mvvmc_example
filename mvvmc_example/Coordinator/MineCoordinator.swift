//
//  MineCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class MineCoordinator {
  
  weak var parentCoordinator: Coordinator?
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  var valudData = MineValueData()
  let barItemType: HomeViewModel.BarItemType
  
  init(parentCoordinator: Coordinator, barItemType: HomeViewModel.BarItemType) {

    self.barItemType = barItemType
    self.parentCoordinator = parentCoordinator
    
    setupNavigationBar()
  }
  
  func setupNavigationBar() {
    navigationController?.navigationBar.isTranslucent = false
    navigationController?.tabBarItem.title = barItemType.title
    navigationController?.tabBarItem.image = UIImage(named: barItemType.title)
  }
}

extension MineCoordinator: Coordinator {
  
  func start() {
    let vc = MineIndexViewController
      .instantiate("Mine")
      .bindViewModel(MineIndexViewModel().bindCoordinator(self))

    navigationController?.setViewControllers([vc], animated: false)
  }
  
  func toNext() {
    let nextVC = MineChangeValueViewController
      .instantiate("Mine")
      .bindViewModel(MineChangeValueViewModel(data: valudData).bindCoordinator(self))
      .bindLeftBarButtonItems(
        UIBarButtonItem(image: UIImage(named: "ic_Nav_Back"), style: .plain, target: nil, action: nil))
    nextVC.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(nextVC, animated: true)
  }
  
  func toPrevious() {
    navigationController?.popViewController(animated: true)
  }
}
