//
//  LeftBarButtonItemable.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/6.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

@objc protocol LeftBarButtonItemable: class {
  var leftBarButtonItems: [UIBarButtonItem] { get set }
  func pressedLeftBarButtonItems(_ sender: UIBarButtonItem)
}

extension LeftBarButtonItemable where Self: UIViewController {

  func bindLeftBarButtonItems(_ leftBarButtonItems: UIBarButtonItem...) -> Self {
    _ = leftBarButtonItems.map { [weak self] (barButtonItem) in
      guard let self = self else { return }
      barButtonItem.target = self
      barButtonItem.action = #selector(pressedLeftBarButtonItems(_:))
    }
    self.leftBarButtonItems = leftBarButtonItems
    self.navigationItem.leftBarButtonItems = leftBarButtonItems
    return self
  }
}
