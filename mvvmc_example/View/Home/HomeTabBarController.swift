//
//  HomeTabBarController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
  
  var viewModel: HomeViewModel?
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
  }
}

extension HomeTabBarController: Storyboarded { }
extension HomeTabBarController: ViewModelable { }
