//
//  AccountIndexViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class AccountIndexViewModel: BaseViewModel {

  weak var coordinator: AccountCoordinator?
}

extension AccountIndexViewModel: Coordinated { }
