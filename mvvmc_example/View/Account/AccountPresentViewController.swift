//
//  AccountIPresentViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class AccountPresentViewController: UIViewController {
  
  @IBOutlet weak var coordinatorLabel: UILabel!
  var viewModel: AccountPresentViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }

  @IBAction func pressedDismissButton(_ sender: UIButton) {
    dismiss(animated: true)
  }
  
  @IBAction func pressedLogoutButton(_ sender: UIButton) {
    AppDelegate.shared?.appCoordinator?.switchLoginCoordinator()
  }
}

extension AccountPresentViewController: Storyboarded { }
extension AccountPresentViewController: ViewModelable { }
