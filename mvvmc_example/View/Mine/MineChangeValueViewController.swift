//
//  MineChangeValueViewController.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class MineChangeValueViewController: UIViewController {
  
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var coordinatorLabel: UILabel!
  var leftBarButtonItems: [UIBarButtonItem] = []
  var viewModel: MineChangeValueViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    valueLabel.text = viewModel?.valueText
  }
  
  @IBAction func pressedPlusButton(_ sender: UIButton) {
    viewModel?.plus()
    valueLabel.text = viewModel?.valueText
  }
  
  @IBAction func pressedMinusButton(_ sender: UIButton) {
    viewModel?.minus()
    valueLabel.text = viewModel?.valueText
  }
}

extension MineChangeValueViewController: Storyboarded { }
extension MineChangeValueViewController: ViewModelable { }
extension MineChangeValueViewController: LeftBarButtonItemable {
  
  func pressedLeftBarButtonItems(_ sender: UIBarButtonItem) {
    viewModel?.updateCoordinatorData()
    viewModel?.coordinator?.toPrevious()
  }
}
