//
//  AppCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class AppCoordinator {
  
  weak var parentCoordinator: Coordinator?
  
  var window: UIWindow
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController?
 
  init(window: UIWindow) {
    self.window = window
  }
  
  func switchHomeCoordinator() {
    freeAllChildCoordinators()
    startChildCoordinator(HomeCoordinator(window: window,
                                          parentCoordinator: self))
  }
  
  func switchLoginCoordinator() {
    freeAllChildCoordinators()
    startChildCoordinator(LoginCoordinator(window: window,
                                           parentCoordinator: self))
  }
}

extension AppCoordinator: Coordinator {

  func start() {
    switchLoginCoordinator()
  }
  
  func toNext() {
  }
  
  func toPrevious() {
  }
}
