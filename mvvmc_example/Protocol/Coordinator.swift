//
//  Coordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

protocol Coordinator: class {
  
  var childCoordinators: [Coordinator] { get set }
  var parentCoordinator: Coordinator? { get set }
  var navigationController: UINavigationController? { get set }
  
  func start()
  func toNext()
  func toPrevious()
}

extension Coordinator {
  
  func startChildCoordinator<T: Coordinator>(_ child: T) {
    childCoordinators.append(child)
    child.start()
  }
  
  func free(child: Coordinator) {
    for (offset, coordinator) in childCoordinators.enumerated() {
      if coordinator !== child { continue }
      coordinator.freeAllChildCoordinators()
      childCoordinators.remove(at: offset)
    }
  }
  
  func freeAllChildCoordinators() {
    for childCoordinator in childCoordinators {
      childCoordinator.parentCoordinator?.free(child: childCoordinator)
    }
  }
  
  // MARK: For Example Property
  var className: String {
    return String(describing: Self.self)
  }
}
