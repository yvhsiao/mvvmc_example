//
//  TransferMoneyIndexViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class TransferMoneyIndexViewController: UIViewController {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  var viewModel: TransferMoneyIndexViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  @IBAction func pressedActionButton(_ sender: UIButton) {
    viewModel?.coordinator?.toNext()
  }
}

extension TransferMoneyIndexViewController: Storyboarded { }
extension TransferMoneyIndexViewController: ViewModelable { }
