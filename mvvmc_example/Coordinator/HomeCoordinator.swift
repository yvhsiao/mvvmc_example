//
//  HomeCoordinator.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/8.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class HomeCoordinator {
  
  weak var parentCoordinator: Coordinator?

  private var window: UIWindow
  private var tabBarController: UITabBarController?
  
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = nil
  var navigationControllers: [UINavigationController] = []
  
  init(window: UIWindow, parentCoordinator: Coordinator) {
    self.window = window
    self.parentCoordinator = parentCoordinator
  }
  
  private func makeChildCoordinators() -> [Coordinator] {
    return HomeViewModel.BarItemType.items
      .compactMap({ [weak self] (barItemType) -> Coordinator? in
        guard let self = self else { return nil }
        switch barItemType {
        case .account:
          return AccountCoordinator(parentCoordinator: self, barItemType: barItemType)
        case .transferMoney:
          return TransferMoneyCoordinator(parentCoordinator: self, barItemType: barItemType)
        case .more:
          return MoreCoordinator(parentCoordinator: self, barItemType: barItemType)
        case .mine:
          return MineCoordinator(parentCoordinator: self, barItemType: barItemType)
        }
      })
  }
  
  private func setupTabBarController() {
    tabBarController = HomeTabBarController
      .instantiate("Home")
      .bindViewModel(HomeViewModel().bindCoordinator(self))
    tabBarController?.tabBar.isTranslucent = false
    tabBarController?.tabBar.barTintColor = .white
    tabBarController?.tabBar.tintColor = .black
    tabBarController?.setViewControllers(navigationControllers, animated: false)
  }
}

extension HomeCoordinator: Coordinator {
  
  func start() {
    _ = makeChildCoordinators()
      .map { [weak self] (coordinator) in
        guard let self = self,
          let childCoordinatorNavigationController = coordinator.navigationController else { return }
        coordinator.start()
        self.childCoordinators.append(coordinator)
        self.navigationControllers.append(childCoordinatorNavigationController)
    }
    setupTabBarController()
    window.rootViewController = tabBarController
    window.makeKeyAndVisible()
  }
  
  func toNext() { }
  
  func toPrevious() { }
}
