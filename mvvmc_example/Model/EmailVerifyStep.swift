//
//  EmailVerifyStep.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/8.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

enum EmailVerifyStep: Int {

  case landing
  case verify
  case result
  
  var stepNumber: Int {
    rawValue + 1
  }
  
  var isLastStep: Bool {
    self == .result
  }
  
  var title: String {
    switch self {
    case .landing: return "EmailVerify-Landing"
    case .verify:  return "EmailVerify-Verify"
    case .result:  return "EmailVerify-Result"
    }
  }
}
