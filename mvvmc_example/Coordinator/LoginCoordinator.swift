//
//  LoginCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class LoginCoordinator {
  
  weak var parentCoordinator: Coordinator?
  
  var window: UIWindow
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  private var hadEmailVerified = false
  private var hadShowWelcomeVC = false
  
  init(window: UIWindow, parentCoordinator: Coordinator) {
    self.window = window
    self.parentCoordinator = parentCoordinator
  }
  
  func startEmailVerifyCoordinator() {
    startChildCoordinator(EmailVerifyCoordinator(parentCoordinator: self))
  }
  
  private func makeEmailVerifyDestinationVC(step: EmailVerifyStep) -> UIViewController {
    return EmailVerifyViewController
      .instantiate("EmailVerify")
      .bindViewModel(EmailVerifyViewModel(step: step).bindCoordinator(self))
      .bindLeftBarButtonItems(UIBarButtonItem(image: UIImage(named: "ic_Nav_Back"),
                                              style: .plain,
                                              target: nil,
                                              action: nil))
  }
}

extension LoginCoordinator: Coordinator {
  
  func start() {
    let vc = LoginViewController
      .instantiate("Login")
      .bindViewModel(LoginViewModel().bindCoordinator(self))
    navigationController?.setViewControllers([vc], animated: false)
    window.rootViewController = navigationController
    window.makeKeyAndVisible()
  }
  
  func toNext() {
    
    if !hadEmailVerified {
      let emailVerifyStep1VC = makeEmailVerifyDestinationVC(step: .landing)
      navigationController?.pushViewController(emailVerifyStep1VC, animated: true)
      return
    }
    
    if !hadShowWelcomeVC {
      let welcomeVC = WelcomeViewController.instantiate("Login")
        .bindViewModel(WelcomeViewModel().bindCoordinator(self))
      navigationController?.setViewControllers([welcomeVC], animated: true)
      hadShowWelcomeVC = true
      return
    }
    
    AppDelegate.shared?.appCoordinator?.switchHomeCoordinator()
  }
  
  func toPrevious() {
    navigationController?.popViewController(animated: true)
  }
}

extension LoginCoordinator: EmailVerifyCoordinatorDelegate {

  func pressedEmailVerifyStep1NextButton() {
    navigationController?.pushViewController(makeEmailVerifyDestinationVC(step: .verify),
                                             animated: true)
  }
  
  func pressedEmailVerifyStep1LeftBarButtonItem() {
    navigationController?.popViewController(animated: true)
  }
  
  func pressedEmailVerifyStep2NextButton() {
    navigationController?.pushViewController(makeEmailVerifyDestinationVC(step: .result),
                                             animated: true)
  }
  
  func pressedEmailVerifyStep3NextButton() {
    hadEmailVerified = true
    toNext()
  }
}
