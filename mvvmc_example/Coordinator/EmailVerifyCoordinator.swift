//
//  EmailVerifyCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/5.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class EmailVerifyCoordinator {
  
  weak var parentCoordinator: Coordinator?
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  var currentStep: EmailVerifyStep = .landing
  
  init(parentCoordinator: Coordinator) {
    self.parentCoordinator = parentCoordinator
  }
  
  private func transitionToNextVC(nextStep: EmailVerifyStep) {
    let destinationVC = makeDestinationVC(step: nextStep)
    navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  private func makeDestinationVC(step: EmailVerifyStep) -> UIViewController {
    return EmailVerifyViewController
      .instantiate("EmailVerify")
      .bindViewModel(EmailVerifyViewModel(step: step).bindCoordinator(self))
      .bindLeftBarButtonItems(UIBarButtonItem(image: UIImage(named: "ic_Nav_Down"), style: .plain, target: nil, action: nil))
  }
}

extension EmailVerifyCoordinator: Coordinator {
  
  func start() {
    guard let navigationController = navigationController else { return }
    navigationController.setViewControllers([makeDestinationVC(step: .landing)], animated: false)
    navigationController.modalPresentationStyle = .fullScreen
    parentCoordinator?.navigationController?.present(navigationController, animated: true)
  }
  
  func toNext() {
    guard let nextStep = EmailVerifyStep(rawValue: currentStep.rawValue + 1) else {
      complete()
      return
    }
    currentStep = nextStep
    transitionToNextVC(nextStep: nextStep)
  }
  
  func toPrevious() {
    guard let previousStep = EmailVerifyStep(rawValue: currentStep.rawValue - 1) else {
      complete()
      return
    }
    currentStep = previousStep
    navigationController?.popViewController(animated: true)
  }
  
  func complete() {
    navigationController?.dismiss(animated: true)
    parentCoordinator?.free(child: self)
  }
}

extension EmailVerifyCoordinator: EmailVerifyCoordinatorDelegate {
  
  func pressedEmailVerifyStep1NextButton() {
    toNext()
  }
  
  func pressedEmailVerifyStep1LeftBarButtonItem() {
    complete()
  }
  
  func pressedEmailVerifyStep2NextButton() {
    toNext()
  }
  
  func pressedEmailVerifyStep3NextButton() {
    toNext()
  }
}
