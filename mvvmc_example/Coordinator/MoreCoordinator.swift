//
//  MoreCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class MoreCoordinator {
  
  weak var parentCoordinator: Coordinator?
  
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  
  private let barItemType: HomeViewModel.BarItemType
  
  init(parentCoordinator: Coordinator, barItemType: HomeViewModel.BarItemType) {

    self.barItemType = barItemType
    self.parentCoordinator = parentCoordinator
    
    setupNavigationBar()
  }
  
  func setupNavigationBar() {
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationController?.navigationBar.shadowImage = UIImage()
    navigationController?.navigationBar.isTranslucent = true
    navigationController?.navigationBar.tintColor = .black
    navigationController?.navigationBar.superview?.backgroundColor = .clear
    navigationController?.tabBarItem.title = barItemType.title
    navigationController?.tabBarItem.image = UIImage(named: barItemType.title)
  }
}

extension MoreCoordinator: Coordinator {

  func start() {
    let vc = MoreIndexViewController
      .instantiate("More")
      .bindViewModel(MoreIndexViewModel().bindCoordinator(self))

    navigationController?.setViewControllers([vc], animated: false)
  }
  
  func toNext() {
    let nextVC = MoreInvisiblePushViewController
      .instantiate("More")
      .bindViewModel(MoreInvisiblePushViewModel().bindCoordinator(self))
      .bindLeftBarButtonItems(
        UIBarButtonItem(image: UIImage(named: "ic_Nav_Back"), style: .plain, target: nil, action: nil))
    nextVC.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(nextVC, animated: true)
  }
  
  func toPrevious() {
    navigationController?.popViewController(animated: true)
  }
}
