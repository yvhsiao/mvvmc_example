//
//  MoreInvisiblePushViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class MoreInvisiblePushViewController: UIViewController {
  
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  var viewModel: MoreInvisiblePushViewModel?
  var leftBarButtonItems: [UIBarButtonItem] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
}

extension MoreInvisiblePushViewController: Storyboarded { }
extension MoreInvisiblePushViewController: ViewModelable { }

extension MoreInvisiblePushViewController: LeftBarButtonItemable {
  
  func pressedLeftBarButtonItems(_ sender: UIBarButtonItem) {
    viewModel?.coordinator?.toPrevious()
  }
}
