//
//  HomeViewModel.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/8.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class HomeViewModel: BaseViewModel {

  weak var coordinator: HomeCoordinator?
}

extension HomeViewModel: Coordinated { }

extension HomeViewModel {

  enum BarItemType: Int {
    case account
    case transferMoney
    case more
    case mine
    
    var title: String {
      switch self {
      case .account: return "帳戶"
      case .transferMoney: return "轉帳"
      case .more: return "更多"
      case .mine: return "我的"
      }
    }
    
    var imageName: String {
      switch self {
      case .account: return ""
      case .transferMoney: return ""
      case .more: return ""
      case .mine: return ""
      }
    }
    
    static var items: [Self] {
      var items: [Self] = []
      var index = 0
      while let item = BarItemType(rawValue: index) {
        items.append(item)
        index = index + 1
      }
      return items
    }
  }
}
