//
//  MoreIndexViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class MoreIndexViewController: UIViewController {
  
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  var viewModel: MoreIndexViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  @IBAction func pressedInvisiblePushButton(_ sender: UIButton) {
    viewModel?.coordinator?.toNext()
  }
}

extension MoreIndexViewController: Storyboarded { }
extension MoreIndexViewController: ViewModelable { }
