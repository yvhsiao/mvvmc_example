//
//  MineChangeValueViewModel.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class MineChangeValueViewModel: BaseViewModel {
  
  private var data: MineValueData
  weak var coordinator: MineCoordinator?
  
  var valueText: String {
    String(data.value)
  }
  
  init(data: MineValueData) {
    self.data = data
  }
  
  func plus() {
    data.value += 1
  }
  
  func minus() {
    data.value -= 1
  }
  
  func updateCoordinatorData() {
    coordinator?.valudData = data
  }
}

extension MineChangeValueViewModel: Coordinated { }
