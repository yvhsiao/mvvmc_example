//
//  EmailVerifyViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/5.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class EmailVerifyViewModel: BaseViewModel {
  
  let step: EmailVerifyStep
  weak var coordinator: EmailVerifyCoordinatorType?
  
  var titleText: String {
    step.title
  }
  
  var stepText: String {
    String(format: "Step %d", step.stepNumber)
  }
  
  var nextButtonTitleText: String {
    step.isLastStep ? "完成" : "下一步"
  }
  
  init(step: EmailVerifyStep) {
    self.step = step
  }
  
  func toNext() {
    switch step {
    case .landing: coordinator?.pressedEmailVerifyStep1NextButton()
    case .verify: coordinator?.pressedEmailVerifyStep2NextButton()
    case .result: coordinator?.pressedEmailVerifyStep3NextButton()
    }
  }
}

extension EmailVerifyViewModel: Coordinated { }
