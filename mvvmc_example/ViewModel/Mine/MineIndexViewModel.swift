//
//  MineIndexViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class MineIndexViewModel: BaseViewModel {
  
  weak var coordinator: MineCoordinator?
  
  var valueText: String {
    String(coordinator?.valudData.value ?? 0)
  }
}

extension MineIndexViewModel: Coordinated { }
