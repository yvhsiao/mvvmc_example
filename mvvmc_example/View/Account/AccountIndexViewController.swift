//
//  AccountIndexViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class AccountIndexViewController: UIViewController {

  @IBOutlet weak var coordinatorLabel: UILabel!
  var viewModel: AccountIndexViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  @IBAction func pressedPresentVCButton(_ sender: UIButton) {
    guard let presentVC = viewModel?.coordinator?.makeAccountPresentVC() else { return }
    present(presentVC, animated: true)
  }
}

extension AccountIndexViewController: Storyboarded { }
extension AccountIndexViewController: ViewModelable { }
