//
//  TransferMoneyCoordinator.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit
import Foundation

class TransferMoneyCoordinator {
  
  weak var parentCoordinator: Coordinator?
  
  var childCoordinators: [Coordinator] = []
  var navigationController: UINavigationController? = UINavigationController()
  
  private let barItemType: HomeViewModel.BarItemType
  
  init(parentCoordinator: Coordinator, barItemType: HomeViewModel.BarItemType) {

    self.barItemType = barItemType
    self.parentCoordinator = parentCoordinator
    
    setupNavigationBar()
  }
  
  func setupNavigationBar() {
    navigationController?.navigationBar.isTranslucent = false
    navigationController?.tabBarItem.title = barItemType.title
    navigationController?.tabBarItem.image = UIImage(named: barItemType.title)
  }
}

extension TransferMoneyCoordinator: Coordinator {

  func start() {
    
    let vc = TransferMoneyIndexViewController
      .instantiate("TransferMoney")
      .bindViewModel(TransferMoneyIndexViewModel().bindCoordinator(self))
    navigationController?.setViewControllers([vc], animated: false)
  }
  
  func toNext() {
    let nextVC = TransferMoneyPushViewController
      .instantiate("TransferMoney")
      .bindViewModel(TransferMoneyPushViewModel().bindCoordinator(self))
      .bindLeftBarButtonItems(UIBarButtonItem(image: UIImage(named: "ic_Nav_Back"), style: .plain, target: nil, action: nil))
    nextVC.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(nextVC, animated: true)
  }
  
  func toPrevious() {
    navigationController?.popViewController(animated: true)
  }
}
