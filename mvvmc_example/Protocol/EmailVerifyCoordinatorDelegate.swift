//
//  EmailVerifyCoordinatorDelegate.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/6.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

typealias EmailVerifyCoordinatorType = Coordinator & EmailVerifyCoordinatorDelegate

protocol EmailVerifyCoordinatorDelegate: class {
  
  func pressedEmailVerifyStep1NextButton()
  func pressedEmailVerifyStep1LeftBarButtonItem()
  func pressedEmailVerifyStep2NextButton()
  func pressedEmailVerifyStep3NextButton()
}
