//
//  EmailVerifyViewController.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/5.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class EmailVerifyViewController: UIViewController {
  
  @IBOutlet weak var nextButton: UIButton!
  
  @IBOutlet weak var stepLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  var viewModel: EmailVerifyViewModel?
  var leftBarButtonItems: [UIBarButtonItem] = []

  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  @IBAction func pressedNextButton(_ sender: UIButton) {
    viewModel?.toNext()
  }
  
  @objc func pressedLeftBarButtonItems(_ sender: UIBarButtonItem) {
    viewModel?.coordinator?.pressedEmailVerifyStep1LeftBarButtonItem()
  }
  
  private func setupUI() {
    stepLabel.text = viewModel?.stepText
    titleLabel.text = viewModel?.titleText
    nextButton.setTitle(viewModel?.nextButtonTitleText ?? "", for: .normal)
    coordinatorLabel.text = viewModel?.coordinator?.className
    nextButton.backgroundColor = (viewModel?.step.isLastStep ?? false) ? .black : .systemYellow
    nextButton.setTitleColor((viewModel?.step.isLastStep ?? false) ? .white : .black, for: .normal)
  }
}

extension EmailVerifyViewController: Storyboarded { }
extension EmailVerifyViewController: ViewModelable { }
extension EmailVerifyViewController: LeftBarButtonItemable { }
