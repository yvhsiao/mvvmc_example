//
//  AppDelegate.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var appCoordinator: AppCoordinator?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    setupAppCoordinator()
    appCoordinator?.start()
    return true
  }
  
  private func setupAppCoordinator() {
    appCoordinator = AppCoordinator(window: UIWindow(frame: UIScreen.main.bounds))
    window = appCoordinator?.window
    window?.makeKeyAndVisible()
  }
}

