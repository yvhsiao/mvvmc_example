//
//  MineIndexViewController.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/9.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class MineIndexViewController: UIViewController {
  
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var coordinatorLabel: UILabel!
  
  var viewModel: MineIndexViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    valueLabel.text = viewModel?.valueText
  }
  
  @IBAction func pressedChangedButton(_ sender: UIButton) {
    viewModel?.coordinator?.toNext()
  }
}

extension MineIndexViewController: Storyboarded { }
extension MineIndexViewController: ViewModelable { }
