//
//  MoreInvisiblePushViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/10.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class MoreInvisiblePushViewModel: BaseViewModel {
  
  weak var coordinator: MoreCoordinator?
  
}

extension MoreInvisiblePushViewModel: Coordinated { }
