//
//  LoginViewModel.swift
//  mvvmc_example
//
//  Created by Yvonne Hsiao on 2020/3/4.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class LoginViewModel: BaseViewModel {
  
  weak var coordinator: LoginCoordinator?
}

extension LoginViewModel: Coordinated { }
