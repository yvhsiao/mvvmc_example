//
//  WelcomeViewModel.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/7.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import Foundation

class WelcomeViewModel: BaseViewModel {
  
  weak var coordinator: LoginCoordinator?
}

extension WelcomeViewModel: Coordinated { }
