//
//  WelcomeViewController.swift
//  mvvmc_example
//
//  Created by Yu-An Hsiao on 2020/3/7.
//  Copyright © 2020 Yvonne Hsiao. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
  
  @IBOutlet weak var coordinatorLabel: UILabel!
  var viewModel: WelcomeViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    coordinatorLabel.text = viewModel?.coordinator?.className
  }
  
  @IBAction func pressedNextButton(_ sender: UIButton) {
    viewModel?.coordinator?.toNext()
  }
}

extension WelcomeViewController: Storyboarded { }
extension WelcomeViewController: ViewModelable { }
